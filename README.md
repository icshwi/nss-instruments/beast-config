# NSS labs beast-config
 
This repository is used to store all Alarm Server configurations used by NSS Instruments supported by ICS-HWI.

## Run AWX playbook to update the service with the alarm server configurations
https://torn.tn.esss.lu.se/#/templates/job_template/556?template_search=page_size:20;order_by:name;type:workflow_job_template,job_template
